;;; larp.el --- create SVG cards for daily tasks  -*- lexical-binding: t; -*-

;; Copyright (C) 2018  David O'Toole

;; Author: David O'Toole <dto@xelf.me>
;; Keywords: oop, tools, pda

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(require 'rx)
(require 'cl)
(require 'eieio)
(require 'svg)

(defun random-choose (set)
  (nth (random (length set)) set))

(defun derange (things)
  (let ((len (length things))
	(things2 (coerce things 'vector)))
    (dotimes (n len)
      (rotatef (aref things2 n)
	       (aref things2 (random len))))
    (coerce things2 'list)))

(defmacro defvar* (symbol initvalue &optional docstring)
  `(progn (defvar ,symbol nil docstring)
	  (setf ,symbol ,initvalue)))

(defvar* safe-color-names
  '("cornflower blue" "yellow" "pale green" "violet red"))

(defvar* larp-hex-colors 
  '("#6495ed" "#ffff00" "#98fb98" "#d02090" "#888888"))

(defvar* larp-categories
  '(:mind :body :food :home :other))

(defun larp-color (category)
  (let ((n (position category larp-categories)))
    (if (numberp n)
	;; use last color if too many categories
	(nth (min n (1- (length larp-hex-colors)))
	     larp-hex-colors)
      (car (last larp-hex-colors)))))

(defvar* larp-activity-names
    '((:mind :meditation :reading :news)
      (:body :weights :walking :stretching)
      (:home :self-care :laundry :dishes :tidying :family)
      (:food :breakfast :lunch :dinner :snack)
      (:other :todo :club :gaming :meta)))

(defun larp-random-category ()
  (random-choose larp-categories))

(defun* larp-random-activity-name (&optional (category (larp-random-category)))
  (random-choose (rest (assoc category larp-activity-names))))

(defun larp-activity-class (name)
  (assert (keywordp name))
  (intern (concat
	   "larp-"
	   (substring (symbol-name name) 1))))

(defun larp-activity-object (name &rest args)
  (let ((class (larp-activity-class name)))
    (when (find-class class)
      (apply #'make-instance class args))))

;;; Activity base class

(defclass larp-activity ()
  ((category :initform nil :initarg :category :accessor larp-category)
   (name :initform nil :initarg :name :accessor larp-name)
   (title :initform nil :initarg :title :accessor larp-title)
   (items :initform nil :initarg :items :accessor larp-items)
   (date :initform nil :initarg :date :accessor larp-date)))

(defmethod larp-find-color ((activity larp-activity))
  (larp-color (larp-category activity)))

(defmethod larp-describe ((activity larp-activity))
  (with-current-buffer (get-buffer-create "*larp*")
    (delete-region (point-min) (point-max))
    (insert (propertize (format "%S" activity)
			'face `(:foreground ,(larp-find-color activity))))))

(defmethod larp-print ((activity larp-activity))
  (format "%S" (list (larp-category activity)
		     (larp-name activity)
		     (larp-items activity))))

;;; Mind 

(defclass larp-mind (larp-activity)
  ((category :initform :mind)
   (name :initform :mind)))

(defvar* larp-buddhist-topics
    '("dukkha (suffering)"
      "metta (kindness)"
      "karma (action)"
      "samsara (wandering)"
      "anicca/aniyta (impermanence)"
      "sunyata (emptiness)"
      "anatta/anatman (impersonality)"
      "nibbana/nirvana (extinction)"
      "paticcasamuppada"
      "sikkha (training)"
      "sikkhapada (five precepts)"
      ))

(defclass larp-meditation (larp-mind)
  ((name :initform :meditation)
   (items :initform (list "10 minutes concentration"
			  (random-choose larp-buddhist-topics)))))

(defmethod initialize-instance :after ((larp-mind larp-mind) &key)
  (if (zerop (random 5))
      (setf (larp-name larp-mind) :shamatha
	    (larp-items larp-mind) '("10 minutes sitting meditation"))
    (setf (larp-name larp-mind) :vipassana)))
    

(defclass larp-reading (larp-mind)
  ((name :initform :reading)
   (items :initform '("10 pages reading"))))

;;; Body

(defclass larp-body (larp-activity)
  ((category :initform :body)
   (name :initform :body)))

(defclass larp-weights (larp-body)
  ((title :initform "20 minute weight workout")
   (name :initform :weights)
   (items :initform
	  (random-choose 
	   '(("jumping jacks" "stretching" "3x10 weighted lunges" "3x10 lateral raises" "3x10 seated reverse flyes")
	     ("crunches" "stretching" "3x10 bench press" "3x10 overhead press" "3x10 EZ-curl")
	     ("squats" "stretching" "3x10 deadlift" "3x10 step ups" "3x10 sit ups"))))))
			 
(defclass larp-walking (larp-body)
  ((title :initform "walk around the block")
   (name :initform :walking)
   (items :initform '("half mile or full mile" "stretch"))))

;;; Home

(defclass larp-home (larp-activity)
  ((category :initform :home)
   (name :initform :home)
   (items :initform (list (random-choose '("laundry" "tidying"))
			  (random-choose '("dishes" "self-care"))))))
				     
;;; Food

(defclass larp-food (larp-activity)
  ((category :initform :food)
   (name :initform :food)))

(defun larp-fruit () (random-choose '("banana" "apple" "orange" "peach" "pear")))
(defun larp-vegetable () (random-choose '("peas" "carrots" "corn")))

(defclass larp-breakfast (larp-food)
  ((name :initform :breakfast)
   (items :initform (list (larp-fruit)
			  (random-choose '("oatmeal" "yogurt" "cold cereal" "egg and toast"))))))

(defclass larp-lunch (larp-food)
  ((name :initform :lunch)
   (items :initform (list (larp-fruit)
			  (random-choose '("tuna or salmon salad"
					   "lean lunchmeat"
					   "peanut butter and jelly"
					   "soup and salad"))))))

(defclass larp-dinner (larp-food)
  ((name :initform :dinner)
   (items :initform (list (random-choose '("beef" "chicken" "fish"))
			  (random-choose '("rice" "pasta"))
			  (larp-vegetable)
			  "small dessert"))))

;;; Other

(defclass larp-other (larp-activity)
  ((category :initform :other)
   (name :initform :club)
   (items :initform (random-choose '(("Book club, 7:00pm @ library")
				     ("Game night, 7:00pm  @ home")
				     ("Movie night @ home"))))))

(defclass larp-moonday (larp-activity)
  ((category :initform :other)
   (name :initform :observance)
   (items :initform '("Thursday, February 22, 2018"
		      "Uposatha: Waxing Moon"))))

;;; Topic cards

(defvar *topic-cards* '(
"Getting to know you"
"Listening with compassion"
"Working as a team 
/ Working in pairs "
"Clear goals 
/ Unclear goals"
"The very happiest I ever was"
"The worst day of my life"
"Dealing with difficult people"
"Letting go  
/ Holding on"
"Men’s life stages"
"Forgiving others 
/ Being forgiven"
"Dealing with failure 
/ Dealing with success"
"Bad habits"
"Healthy Friendships 
/ Unhealthy Friendships"
"Difficulty making friends"
"Self-concept"
"Self-esteem"
"Self-actualization"
"Self-image 
/ Distorted self image"
"Negative body image 
/ Positive body image"
"Mid-life passage"
"My teens"
"My twenties"
"My thirties"
"My forties"
"My fifties"
"My sixties"
"Aging"
"Violence"
"Depression"
"Coping with loss and grief"
"Coping with change and uncertainty"
"My mother 
/ My father"
"The male body"
"The men I admired throughout my life history 
/ The women I admired throughout my life history"
"Marriage"
"Fathering"
"Celibacy"
"Loss of a loved one"
"Intimate relationships 
/ Lack of intimacy"
"Emotional distance"
"Sexual expression 
/ Lack of sexual expression"
"Sexual difficulties"
"Sexual experience 
/ Lack of sexual experience "
"Personal distance"
"Social anxiety"
"Body language"
"Passing as straight"
"When did you know...?"
"Coming out"
"Bad breakups 
/ Worse breakups"
"A night to remember 
/ A night to forget"
"Top 
/ Bottom"
"Being a wallflower"
"Memories of puberty"
"Feeling invisible"
"Late blooming"
"My life was affected by a suicide"
"The prostate"
"Changing careers"
"How could you do this to me?"
"Grief 
/ Trauma"
"Post-traumatic stress
/ Post-traumatic growth"
"I don’t want to talk about it 
/ You just don’t understand"
"Fitness and sports"
"Injuries"
"I should’ve done this twenty years ago 
/ I never should have done that"
"Homophobia"
"Beards"
"Monogamy" 
"Why can’t I be more like you 
/ I worry I’ll end up like you"
"Saying the wrong thing"
"Being an uncle"
"Being a son"
"Being a friend"
"Let’s just be friends 
/ “Friends with benefits”"
"I love you as a friend 
/ I don’t love you anymore"
"Friendships with men"
"Understanding women 
/ Not understanding women"
"Conflicts with other men 
/ Harmony with other men"
"Not understanding other men 
/ Not understanding my father"
"Butch 
/ Femme"
"“Not into the scene” 
/ “New to this”"
"Bear 
/ Otter 
/ Twink"
"City gays 
/ country gays"
"“Masc only”"
"Feeling ignored 
/ Feeling scrutinized"
"Second chances"
"Missed opportunities"
"Awesome things about me"))


(defclass larp-topic (larp-activity)
  ((category :initform :mind)
   (name :initform ::)
   (sequence-number :initform 0 :initarg :sequence-number)))

(defmethod initialize-instance :after ((larp-topic larp-topic) &key)
  (setf (larp-title larp-topic) nil)
  (setf (larp-items larp-topic) 
	(split-string (nth (slot-value larp-topic 'sequence-number) *topic-cards*) "\n" t)))
    
;;; SVG Card generation

(defvar* larp-x-offset 0.5)
(defvar* larp-y-offset 0.8)
(defvar* larp-page-width 11.0)
(defvar* larp-page-height 8.5)
(defvar* larp-card-width 2.0)
(defvar* larp-card-height 3.5)
(defvar* larp-card-rows 2)
(defvar* larp-card-columns 5)
(defvar* larp-card-header-height 0.25)
(defun larp-inches (number)
  (format "%Sin" number))

(defun larp-blank-page ()
  (svg-create (larp-inches larp-page-width)
	      (larp-inches larp-page-height)))

(defvar* larp-current-page nil)

(defvar* larp-x 0.0)
(defvar* larp-y 0.0)
(defvar* larp-tx 0.0)
(defvar* larp-ty 0.0)

(defun larp-card-x (column)
  (+ larp-x-offset (* column larp-card-width))) 

(defun larp-card-y (row)
  (+ larp-y-offset (* row larp-card-height)))

(defun larp-reset-page ()
  (setf larp-current-page nil)
  (setf larp-x larp-x-offset)
  (setf larp-y larp-y-offset))

(defmethod larp-draw-card ((activity larp-activity) x y)
  (let ((category (larp-category activity))
	(name (larp-name activity))
	(title (larp-title activity))
	(items (larp-items activity))
	(date (larp-date activity)))
    (svg-rectangle larp-current-page
		   (larp-inches x)
		   (larp-inches y)
		   (larp-inches larp-card-width)
		   (larp-inches larp-card-height)
		   :fill "white")
    (svg-rectangle larp-current-page
		   (larp-inches x)
		   (larp-inches y)
		   (larp-inches larp-card-width)
		   (larp-inches larp-card-header-height)
		   :fill (larp-find-color activity))
    (svg-text larp-current-page
	      (substring (symbol-name name) 1)
	      :font-size "14"
	      :font-weight "bold"
	      :fill "black"
	      :font-family "sans-serif"
	      :x (larp-inches (+ x 0.03))
	      :y (larp-inches (+ y 0.2 larp-card-header-height)))
    (svg-line larp-current-page
	      (larp-inches x)
	      (larp-inches (+ y 0.25 larp-card-header-height))
	      (larp-inches (+ x larp-card-width))
	      (larp-inches (+ y 0.25 larp-card-header-height))
	      :stroke "black")
    (larp-draw-paragraph items x y)))

(defun larp-draw-paragraph (items x y)
  (when items
    (let ((baseline (+ y larp-card-header-height 0.45)))
      (dolist (line items)
	(svg-text larp-current-page
		  line
		  :font-size "10"
		  :fill "black"
		  :font-weight "bold"
		  :font-family "sans-serif"
		  :x (larp-inches (+ x 0.03))
		  :y (larp-inches baseline))
	(incf baseline 0.15)))))

(defun* larp-begin-page ()
  (larp-reset-page)
  (setf larp-current-page (larp-blank-page)))

;;; Testing

;; Here is the main test. You can use the in-emacs svg viewer and M-x
;; auto-revert-mode for live update. This test (and the x-offset
;; y-offset values above) works for Avery 5871 business card sheets.

;; (larp-make-cards "~/larp/test.svg")

;; To print the resulting file, use these shell commands:

;;    inkscape test.svg --export-pdf=test.pdf
;;    lpr test.pdf

(defun larp-make-cards (output-file)
  (larp-begin-page)
  (let ((i 88))
    (dolist (c '(1 2 3 4))
      (larp-draw-card (make-instance 'larp-topic :sequence-number (+ i c)) larp-x larp-y)
      (incf larp-x larp-card-width))
    (setf larp-x larp-x-offset)
    (setf larp-y (+ larp-y-offset larp-card-height))
    (dolist (c '(5 6 7 8))
      (larp-draw-card (make-instance 'larp-topic :sequence-number (+ i c)) larp-x larp-y)
      (incf larp-x larp-card-width))
    (with-temp-buffer
	(svg-print larp-current-page)
      (write-file output-file))))


(provide 'larp)
;;; larp.el ends here
